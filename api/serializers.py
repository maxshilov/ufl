from rest_framework import serializers


class EventSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    type = serializers.IntegerField()
    timestamp = serializers.DateTimeField()


class SearchRequestSerializer(serializers.Serializer):
    user_id = serializers.IntegerField()
    start = serializers.DateTimeField()
    end = serializers.DateTimeField()
