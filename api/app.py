from django.apps.config import AppConfig
from django.conf import settings
from .dyn_array import Stats

stats = None


class APIApp(AppConfig):
    name = 'api'

    def ready(self):
        global stats
        print("APIApp loading")
        stats = Stats(path=settings.STAT_FILE, dtype=[('user_id', 'int64'), ('type', 'uint8'), ('timestamp', 'M8[us]')])
