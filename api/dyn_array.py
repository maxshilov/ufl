import numpy as np
import os


class DynamicArray(object):
    CHUNK_LEN = int(1e5)

    def __init__(self, data=None, size=0, dtype=None):

        if dtype is None:
            dtype = int

        self._data, self._size = (np.zeros(self.CHUNK_LEN, dtype=dtype), 0) if data is None else (data, size)

    @property
    def data(self):
        return self._data[:self.size]

    def append(self, value):
        if len(self._data) == self.size:
            self._data = self._resize(int(self.size * 1.5))

        self._data[self.size] = value
        self.size = self.size + 1

    def _resize(self, new_size):
        self._data = np.resize(self._data, new_size)
        return self._data

    @property
    def size(self):
        return self._size

    @size.setter
    def size(self, size):
        self._size = size


class Stats(DynamicArray):
    scores = np.array([0, 1, 3, 2, 5, 4])

    def __init__(self, path=None, dtype=None):

        if not os.path.isfile(path):
            np.save(path, np.zeros(self.CHUNK_LEN,
                                   dtype=[('user_id', 'int64'), ('type', 'uint8'), ('timestamp', 'M8[us]')]))
            # save payload length
            np.save("%s.meta.npy" % os.path.splitext(path)[0], np.array([0]))

        data = np.load(path, mmap_mode='r+')
        self._length = np.load("%s.meta.npy" % os.path.splitext(path)[0], mmap_mode='r+')

        super(Stats, self).__init__(data, self._length[0], dtype=dtype)

    def search(self, user_id, start, end):
        # filter by user first, to decrease search time
        user_events = np.extract(self._data['user_id'] == user_id, self._data)

        if not user_events.size:
            return dict(events={}, total_score=0)

        # left_ind = np.searchsorted(data['timestamp'], v=start, side='left')
        # right_ind = np.searchsorted(data[left_ind:]['timestamp'], v=end, side='left')  # 84ms total

        events = np.extract(user_events['timestamp'] > start, user_events)
        events = np.extract(events['timestamp'] <= end, events) #98 ms total

        if not events.size:
            return dict(events={}, total_score=0)

        return self.__prepare_stats(events)

    def __prepare_stats(self, events):
        # compute bins. minlength should be 6. for dot product
        type_freq = np.bincount(events['type'], minlength=6)
        # report type frequencies as dict. omit 0 type
        return {'events': dict(enumerate(type_freq[1:], start=1)), 'total_score': type_freq.dot(self.scores)}

    @property
    def size(self):
        return self._length[0]

    @size.setter
    def size(self, size):
        self._length[0] = size
