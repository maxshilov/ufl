from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APILiveServerTestCase


class EventTests(APILiveServerTestCase):
    def test_add_event(self):
        url = reverse('events-api')
        data = dict(user_id=84, type=2, timestamp='2017-06-04T00:00:01.000000')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_search(self):
        url = reverse('events-api')
        data = dict(user_id=84, type=2, timestamp='2017-06-04T00:00:01.000000')

        # create 5 events
        for i in range(5):
            response = self.client.post(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # search
        data = dict(user_id=84, start='2017-06-03T00:00:01.000000', end='2017-07-03T00:00:01.000000')
        response = self.client.get(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data['total_score'] != 0)
        self.assertTrue(response.data['events'][2] != 0)
