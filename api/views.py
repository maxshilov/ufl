from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import EventSerializer, SearchRequestSerializer
from .app import stats


class EventsAPI(APIView):
    authentication_classes = []

    def __init__(self, **kwargs):
        global stats
        self.stats = stats
        super(EventsAPI, self).__init__(**kwargs)

    def get(self, request, format=None):
        """
        Perform search. all parameters are required.
        """
        params = SearchRequestSerializer(data=request.query_params)
        params.is_valid(raise_exception=True)

        return Response(data=self.stats.search(**params.validated_data))

    def post(self, request, format=None):
        event = EventSerializer(data=request.data)
        event.is_valid(raise_exception=True)
        vdata = event.validated_data
        self.stats.append((vdata['user_id'], vdata['type'], vdata['timestamp']))

        return Response(status=status.HTTP_201_CREATED, data='ok')
