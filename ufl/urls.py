from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'events', views.EventsAPI, base_name='events')

urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^api/events/', views.EventsAPI.as_view(), name='events-api'),
]
