### Installation ###
1. Install requirements


```
#!bash

pip install -r requirements.txt
```


2. Run dev server

```
#!bash

python manage.py runserver
```


### tests ###


```
#!bash

python manage.py test
```